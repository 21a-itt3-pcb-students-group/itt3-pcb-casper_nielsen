|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   1  |
|------------------------------------------------------------------------------|
|  Hat_board.brd                                                               |
|                                                    Mon Nov  8 09:46:54 2021  |
|------------------------------------------------------------------------------|
| NET CHANGES                                                                  |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|   net  name |  type of change |                net     pins                  |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
  3.3V         pins ADDED TO this new net
                                 X1.1           X1.17          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  5.0V         pins ADDED TO this new net
                                 X1.4           
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  0            pins ADDED TO this new net
                                 C1.2           C2.2           C3.2           
                                 LED1.1         LED2.1         LED3.1         
                                 LED3.2         RESISTOR.1     SCREEN1.1      
                                 SCREEN1.5      SCREEN1.16     X1.6           
                                 X1.39          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  GND          pins ADDED TO this new net
                                 X1.9           X1.14          X1.20          
                                 X1.25          X1.30          X1.34          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03210       pins ADDED TO this new net
                                 X1.37          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03222       pins ADDED TO this new net
                                 X1.35          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03234       pins ADDED TO this new net
                                 X1.33          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03246       pins ADDED TO this new net
                                 X1.31          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03261       pins ADDED TO this new net
                                 X1.29          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03361       pins ADDED TO this new net
                                 C1.1           LED1.3         RESISTOR.3     
                                 SCREEN1.2      SCREEN1.15     X1.2           
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03372       pins ADDED TO this new net
                                 SCREEN1.14     X1.12          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03379       pins ADDED TO this new net
                                 SCREEN1.13     X1.16          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03386       pins ADDED TO this new net
                                 SCREEN1.12     X1.18          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   2  |
|------------------------------------------------------------------------------|
|  Hat_board.brd                                                               |
|                                                    Mon Nov  8 09:46:54 2021  |
|------------------------------------------------------------------------------|
| NET CHANGES                                                                  |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|   net  name |  type of change |                net     pins                  |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
  N03393       pins ADDED TO this new net
                                 SCREEN1.11     X1.22          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03400       pins ADDED TO this new net
                                 SCREEN1.6      X1.24          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03462       pins ADDED TO this new net
                                 SCREEN1.4      X1.26          
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03533       pins ADDED TO this new net
                                 RESISTOR.2     SCREEN1.3      
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03632       pins ADDED TO this new net
                                 C2.1           LED2.3         
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03643       pins ADDED TO this new net
                                 C3.1           LED3.3         
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03723       pins ADDED TO this new net
                                 LED1.2         LED2.4         
 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -  
  N03735       pins ADDED TO this new net
                                 LED2.2         LED3.4         
|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   3  |
|------------------------------------------------------------------------------|
|  Hat_board.brd                                                               |
|                                                    Mon Nov  8 09:46:54 2021  |
|------------------------------------------------------------------------------|
| COMPONENT DEFINITION added to design                                         |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|    device type                                                               |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
 C_CAP196_0.1U
 LCD_16X2_GPIOPIN_2_LCD_LCD_16X2
 PINBOX_GPIOPINS_1_PINBOX
 POT_VRES10_10K
 SK6812_LED_PACKAGE_SK6812
|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   4  |
|------------------------------------------------------------------------------|
|  Hat_board.brd                                                               |
|                                                    Mon Nov  8 09:46:54 2021  |
|------------------------------------------------------------------------------|
| COMPONENTS ADDED to design                                                   |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|    ref des        |    device type                                           |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
 C1                  C_CAP196_0.1U         
 C2                  C_CAP196_0.1U         
 C3                  C_CAP196_0.1U         
 LED1                SK6812_LED_PACKAGE_SK6812 
 LED2                SK6812_LED_PACKAGE_SK6812 
 LED3                SK6812_LED_PACKAGE_SK6812 
 RESISTOR            POT_VRES10_10K        
 SCREEN1             LCD_16X2_GPIOPIN_2_LCD_LCD_16X2 
 X1                  PINBOX_GPIOPINS_1_PINBOX 
|------------------------------------------------------------------------------|
|                                 ECO   REPORT                                 |
|                                                                    Page   5  |
|------------------------------------------------------------------------------|
|  Hat_board.brd                                                               |
|                                                    Mon Nov  8 09:46:54 2021  |
|------------------------------------------------------------------------------|
| SLOT PROPERTIES added to design                                              |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
|   slot_id    |   x   |   y   |   property   |             value              |
|- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - |
 C1.2                           PRIM_FILE      .\pstchip.dat
 C2.2                           PRIM_FILE      .\pstchip.dat
 C3.2                           PRIM_FILE      .\pstchip.dat
 LED1.1                         PRIM_FILE      .\pstchip.dat
 LED2.1                         PRIM_FILE      .\pstchip.dat
 LED3.1                         PRIM_FILE      .\pstchip.dat
 RESISTOR.2                     PRIM_FILE      .\pstchip.dat
 SCREEN1.1                      PRIM_FILE      .\pstchip.dat
 X1.8                           PRIM_FILE      .\pstchip.dat
|------------------------------------------------------------------------------|
|       Nets changed          :      55                                        |
|       Comp definitions added:       5                                        |
|       Components added      :       9                                        |
|       Slot property added   :       9                                        |
|                                                                              |
|   Total ECO changes reported:      78                                        |
|------------------------------------------------------------------------------|
