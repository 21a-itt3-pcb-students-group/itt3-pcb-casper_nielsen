# ITT3-PCB-Casper_Nielsen

## PCB Project
This Gitlab is to serve a repository for my project of making a Raspberry Pi HAT
From idea to design to Finished PCB

The HAT will have a LCD display and 5 push button switches and 3 NEO-Pixel LEDs

This Gitlab contains documentation for the project and the project report itself
The project is intended to be open source for anyone to look at the ressources and report, to gather inspiration for their own project or to make my PCB and use it for any purpose they see fit

[[_TOC_]]

## Code
Here you can find the code i used to test the push button switches and the code to test the LCD scree:
### [Code folder]
[Button Code] <br>
[LCD Code]

## Components
Here you will find a list of the components used for the PCB in this project
### [Components Folder]

## Exercises
Here you will find som answers for an exercise regarding gathering information about surface mount components
### [Exercises Folder]

## JLCPCB Receipt
Here are the pictures of what the JLCPCB interpreted my Gerber files to
### [JLCPCB Receipt Folder]
The PCB design: [PCB_JLCPCB] <br>
Placement of the [SMD_components]

## KiCad Files
Here you can find all the KiCAD files used in the project
### [KiCad File Folder]
Files sent to JLCPCB for production of my PCB [KiCAD_jcl_files]<br>
Picture of the Assigned Footprints window [Pictures]<br>
A tool i tried to us for xporting BOM files automaticly but it faild [export_BOM_tool]

## OrCAD Files
Here are all the files i used in OrCAD for this Project
### [OrCAD Folder]
[Allgro]<br>
[PCB Scematic]<br>
[Netlist]<br>
[Last PCB Board Attempt]

## Relevant Pictures
### [Pictures from the Project]

## Requirements
### [Requirements]
[Educators Requirements]<br>
[Raspberry Pi Foundation Mechanical Specs]<br>
[Raspberry Pi Foundation Requirements for HAT]<br>

## Sources
### [Sources Folder]
The 3 standard courses i completed [Nordcad Beginnder Course]<br>
Links for usefull SMD information [SMD Links]

## Testing
### [Testing Folder]
Button test code run from a cmd window [Button Test Command Window]<br>
[Completed Test of PCB]<br>
[End Message Test]<br>
[PCB Is Fun Message]

## Report
The final report of my PCB Project [Report]

[greenhouse_folder]: https://gitlab.com/thenillernielsen/caspernielsen/-/tree/main/semester_projects/1st_semester_project_greenhouse

[Code folder]: https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/tree/main/code
[Button Code]: https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/code/Button_code.py
[LCD Code]: https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/code/LCD_code.py
[Components Folder]: https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/tree/main/Components
[Exercises Folder]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/tree/main/Exercises
[JLCPCB Receipt Folder]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/tree/main/JLCPCB_receipt
[PCB_JLCPCB]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/JLCPCB_receipt/PCB_JLCPCB.png
[SMD_components]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/JLCPCB_receipt/SMD_component_JLCPCB.png
[KiCad File Folder]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/tree/main/Kikad_file
[KiCAD_jcl_files]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/tree/main/Kikad_file/KiCAD_jcl_files
[Pictures]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/tree/main/Kikad_file/Pictures
[export_BOM_tool]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/tree/main/Kikad_file/export_BOM_tool/a8c4a4b96907cfccf616a1edb59d0389-2c9191c0f1fe5471db3362ccf29f71d3235af4d4
[OrCAD Folder]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/tree/main/Orcad_files
[Allgro]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/tree/main/Orcad_files/allegro
[PCB Scematic]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/Orcad_files/PCB_Schematic.pdf
[Netlist]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/tree/main/Orcad_files/Netlist
[Last PCB Board Attempt]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/Orcad_files/Boards/4_try.brd
[Pictures from the Project]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/tree/main/Pictures
[Requirements]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/tree/main/Requirements
[Educators Requirements]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/Requirements/educators_requirements.md
[Raspberry Pi Foundation Mechanical Specs]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/Requirements/RPi_foundation_mechanical_specs.pdf
[Raspberry Pi Foundation Requirements for HAT]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/Requirements/RPi_foundation_requirements_HAT.md
[Sources Folder]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/tree/main/Sources
[Nordcad Beginnder Course]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/Sources/Nordcad_beginner_course.md
[SMD Links]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/Sources/SMD_links.md
[Testing Folder]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/tree/main/Testing
[Button Test Command Window]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/Testing/Button_test_cmd.png
[Completed Test of PCB]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/Testing/Completed_test.JPG
[End Message Test]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/Testing/End_message_test.JPG
[PCB Is Fun Message]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/Testing/PCB_is_fun_msg_test.JPG
[Report]:https://gitlab.com/21a-itt3-pcb-students-group/itt3-pcb-casper_nielsen/-/blob/main/Report_PCB.pdf