Source: https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww36

This link is usefull for gathering basic rudementary information about SMD/SMT components: https://www.electronics-notes.com/articles/electronic_components/surface-mount-technology-smd-smt/packages.php

This link introduces what SMT technology is, the historical origin and development process of SMT: https://www.pcbonline.com/blog/pcb-smt-components.html

List of integrated circuit packaging types: the generel wiki, it is very usefull to understand how messurements are given through technical documentation like a datasheet. It can also be used to get a quick reference to technical terms not readily available or known to someone new to PCBs: https://en.wikipedia.org/wiki/List_of_integrated_circuit_packaging_types#PIN-PITCH

The generel wiki for surface mount technology: https://en.wikipedia.org/wiki/Surface-mount_technology

