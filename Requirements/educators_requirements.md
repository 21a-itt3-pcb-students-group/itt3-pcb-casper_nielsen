 A requirement for the circuit you choose is to include either a microprocessor on the circuit or that the circuit is an add-on to a microprocessor system (hat, carrier board etc.) You might already have a circuit that you want to develop into a pcb, if so let me know what the idea and circuit is and i will see if it can be approved according to the learning goals.
Source: https://eal-itt.gitlab.io/21a-itt3-pcb/exercises/exercises_ww34

 This circuit project is about building an add-on PCB (hat) to the Raspberry Pi. The hat needs to follow the official har standard outlined by the Raspberry Pi foundation described here https://github.com/raspberrypi/hats Make sure to also read and understand the design guide https://github.com/raspberrypi/hats/blob/master/designguide.md

## The main requirements when building this circuit into a PCB is to have:
* Use SMT components for everything possible
* RPi Hat according to official hat standard
* Several sensors onboard (accelerometer, IMU, temperature sensor etc.)
* Other functionality that you decide
Source: https://eal-itt.gitlab.io/21a-itt3-pcb/other-docs/circuit_catalogue.html