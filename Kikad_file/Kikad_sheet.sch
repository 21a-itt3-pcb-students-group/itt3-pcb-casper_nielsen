EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Switch:SW_Push SW2
U 1 1 6189314D
P 3750 4400
F 0 "SW2" H 3750 4685 50  0000 C CNN
F 1 "SW_Push" H 3750 4594 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 3750 4600 50  0001 C CNN
F 3 "~" H 3750 4600 50  0001 C CNN
F 4 "C127509" H 3750 4400 50  0001 C CNN "LCSC"
	1    3750 4400
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW3
U 1 1 61893946
P 3750 4850
F 0 "SW3" H 3750 5135 50  0000 C CNN
F 1 "SW_Push" H 3750 5044 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 3750 5050 50  0001 C CNN
F 3 "~" H 3750 5050 50  0001 C CNN
F 4 "C127509 " H 3750 4850 50  0001 C CNN "LCSC"
	1    3750 4850
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW4
U 1 1 61893F23
P 3750 5300
F 0 "SW4" H 3750 5585 50  0000 C CNN
F 1 "SW_Push" H 3750 5494 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 3750 5500 50  0001 C CNN
F 3 "~" H 3750 5500 50  0001 C CNN
F 4 "C127509" H 3750 5300 50  0001 C CNN "LCSC"
	1    3750 5300
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW5
U 1 1 61894C3B
P 3750 5800
F 0 "SW5" H 3750 6085 50  0000 C CNN
F 1 "SW_Push" H 3750 5994 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 3750 6000 50  0001 C CNN
F 3 "~" H 3750 6000 50  0001 C CNN
F 4 "C127509 " H 3750 5800 50  0001 C CNN "LCSC"
	1    3750 5800
	1    0    0    -1  
$EndComp
Text GLabel 4850 5900 0    50   Input ~ 0
GND
Text GLabel 4850 5800 0    50   Input ~ 0
GPIO26
Text GLabel 4850 5700 0    50   Input ~ 0
GPIO19
Text GLabel 4850 5600 0    50   Input ~ 0
GPIO13
Text GLabel 4850 5500 0    50   Input ~ 0
GPIO6
Text GLabel 4850 5400 0    50   Input ~ 0
GPIO5
$Comp
L Connector_Generic:Conn_01x16 J2
U 1 1 618AB12D
P 6500 2500
F 0 "J2" V 6717 2446 50  0000 C CNN
F 1 "Conn_01x16" V 6626 2446 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 6500 2500 50  0001 C CNN
F 3 "~" H 6500 2500 50  0001 C CNN
	1    6500 2500
	0    -1   -1   0   
$EndComp
Text GLabel 5350 4000 2    50   Input ~ 0
5V1
Text GLabel 5350 4100 2    50   Input ~ 0
5V2
Text GLabel 5350 4200 2    50   Input ~ 0
GND
Text GLabel 5350 4300 2    50   Input ~ 0
TXD
Text GLabel 5350 4400 2    50   Input ~ 0
RXD
Text GLabel 5350 4500 2    50   Input ~ 0
GPIO18
Text GLabel 5350 4600 2    50   Input ~ 0
GND
Text GLabel 5350 4700 2    50   Input ~ 0
GPIO23
Text GLabel 5350 4800 2    50   Input ~ 0
GPIO24
Text GLabel 5350 4900 2    50   Input ~ 0
GND
Text GLabel 5350 5000 2    50   Input ~ 0
GPIO25
Text GLabel 5350 5100 2    50   Input ~ 0
CE0
Text GLabel 5350 5200 2    50   Input ~ 0
CE1
Text GLabel 5800 3600 3    50   Input ~ 0
GND
Text GLabel 3250 6250 3    50   Input ~ 0
GND
Text GLabel 10950 5900 3    50   Input ~ 0
GND
$Comp
L Device:C C3
U 1 1 618B1DB2
P 9650 4450
F 0 "C3" V 9398 4450 50  0000 C CNN
F 1 "0.1u" V 9489 4450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 9688 4300 50  0001 C CNN
F 3 "~" H 9650 4450 50  0001 C CNN
F 4 "C2655418" V 9650 4450 50  0001 C CNN "LCSC"
	1    9650 4450
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 618B3FC9
P 8400 4450
F 0 "C2" V 8148 4450 50  0000 C CNN
F 1 "0.1u" V 8239 4450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 8438 4300 50  0001 C CNN
F 3 "~" H 8400 4450 50  0001 C CNN
F 4 "C2655418" V 8400 4450 50  0001 C CNN "LCSC"
	1    8400 4450
	0    1    1    0   
$EndComp
$Comp
L Device:C C1
U 1 1 618B507A
P 6950 4450
F 0 "C1" V 6698 4450 50  0000 C CNN
F 1 "0.1u" V 6789 4450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0402_1005Metric" H 6988 4300 50  0001 C CNN
F 3 "~" H 6950 4450 50  0001 C CNN
F 4 "C2655418" V 6950 4450 50  0001 C CNN "LCSC"
	1    6950 4450
	0    1    1    0   
$EndComp
Wire Wire Line
	9500 4950 9300 4950
Wire Wire Line
	9300 4950 9300 4450
Wire Wire Line
	9300 4450 9500 4450
Wire Wire Line
	10100 5150 10300 5150
Wire Wire Line
	10300 5150 10300 5900
Wire Wire Line
	9800 4450 10400 4450
Wire Wire Line
	10400 4450 10400 5900
Wire Wire Line
	10300 5900 10400 5900
Connection ~ 10400 5900
Wire Wire Line
	10400 5900 10950 5900
Wire Wire Line
	10100 4950 10950 4950
Wire Wire Line
	10950 4950 10950 5900
Wire Wire Line
	9500 5150 9150 5150
Wire Wire Line
	9150 5150 9150 4950
Wire Wire Line
	9150 4950 8750 4950
Wire Wire Line
	8150 5150 7700 5150
Wire Wire Line
	7700 5150 7700 4950
Wire Wire Line
	7700 4950 7300 4950
Wire Wire Line
	10300 5900 9050 5900
Wire Wire Line
	7550 4450 7100 4450
Connection ~ 10300 5900
Wire Wire Line
	8750 5150 8900 5150
Wire Wire Line
	8900 5150 8900 5900
Connection ~ 8900 5900
Wire Wire Line
	8900 5900 7550 5900
Wire Wire Line
	8150 4950 8050 4950
Wire Wire Line
	8050 4950 8050 4450
Wire Wire Line
	8050 4450 8250 4450
Wire Wire Line
	8550 4450 9050 4450
Wire Wire Line
	9050 4450 9050 5900
Connection ~ 9050 5900
Wire Wire Line
	9050 5900 8900 5900
Wire Wire Line
	7300 5150 7550 5150
Wire Wire Line
	7550 4450 7550 5150
Connection ~ 7550 5150
Wire Wire Line
	7550 5150 7550 5900
Wire Wire Line
	6700 4950 6500 4950
Wire Wire Line
	6500 4950 6500 4450
Wire Wire Line
	6500 4450 6800 4450
Wire Wire Line
	3550 4000 3250 4000
Wire Wire Line
	3250 4000 3250 4400
Wire Wire Line
	3550 4400 3250 4400
Connection ~ 3250 4400
Wire Wire Line
	3250 4400 3250 4850
Wire Wire Line
	3550 4850 3250 4850
Connection ~ 3250 4850
Wire Wire Line
	3250 4850 3250 5300
Wire Wire Line
	3550 5300 3250 5300
Connection ~ 3250 5300
Wire Wire Line
	3250 5300 3250 5800
Wire Wire Line
	3550 5800 3250 5800
Connection ~ 3250 5800
Wire Wire Line
	3250 5800 3250 6250
Text GLabel 3950 4000 2    50   Input ~ 0
GPIO5
Text GLabel 3950 4400 2    50   Input ~ 0
GPIO6
Text GLabel 3950 4850 2    50   Input ~ 0
GPIO13
Text GLabel 3950 5300 2    50   Input ~ 0
GPIO19
Text GLabel 3950 5800 2    50   Input ~ 0
GPIO26
$Comp
L Device:R_POT_TRIM_US RV1
U 1 1 618CBE9D
P 5450 3100
F 0 "RV1" V 5245 3100 50  0000 C CNN
F 1 "R_POT_TRIM_US" V 5336 3100 50  0000 C CNN
F 2 "Potentiometer_THT:Potentiometer_Bourns_3386F_Vertical" H 5450 3100 50  0001 C CNN
F 3 "~" H 5450 3100 50  0001 C CNN
	1    5450 3100
	0    1    1    0   
$EndComp
Wire Wire Line
	5800 2700 5800 2850
Wire Wire Line
	5800 2850 5150 2850
Wire Wire Line
	5150 2850 5150 3100
Wire Wire Line
	5150 3100 5300 3100
Wire Wire Line
	5800 2850 5800 3350
Connection ~ 5800 2850
Wire Wire Line
	5450 3250 6000 3250
Wire Wire Line
	5600 3100 5900 3100
Wire Wire Line
	5900 3100 5900 2700
Wire Wire Line
	5800 3350 6200 3350
Wire Wire Line
	6200 3350 6200 2700
Connection ~ 5800 3350
Wire Wire Line
	5800 3350 5800 3500
Wire Wire Line
	5800 3500 7300 3500
Wire Wire Line
	7300 3500 7300 2700
Connection ~ 5800 3500
Wire Wire Line
	5800 3500 5800 3600
Wire Wire Line
	6000 3250 6000 2700
Wire Wire Line
	5900 3100 5900 3450
Wire Wire Line
	5900 4950 6500 4950
Connection ~ 5900 3100
Connection ~ 6500 4950
Text GLabel 5300 3450 0    50   Input ~ 0
5V1
Wire Wire Line
	5300 3450 5900 3450
Connection ~ 5900 3450
Wire Wire Line
	5900 3450 5900 4950
Wire Wire Line
	5900 3450 7200 3450
Wire Wire Line
	7200 3450 7200 2700
Text GLabel 7100 2700 3    50   Input ~ 0
GPIO18
Text GLabel 7000 2700 3    50   Input ~ 0
GPIO23
Text GLabel 6900 2700 3    50   Input ~ 0
GPIO24
Text GLabel 6800 2700 3    50   Input ~ 0
GPIO25
Text GLabel 6300 2700 3    50   Input ~ 0
CE0
Text GLabel 6100 2700 3    50   Input ~ 0
CE1
$Comp
L Connector_Generic:Conn_02x20_Odd_Even J1
U 1 1 618DD4B3
P 5050 4900
F 0 "J1" H 5100 6017 50  0000 C CNN
F 1 "Conn_02x20_Odd_Even" H 5100 5926 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x20_P2.54mm_Vertical" H 5050 4900 50  0001 C CNN
F 3 "~" H 5050 4900 50  0001 C CNN
	1    5050 4900
	1    0    0    -1  
$EndComp
$Comp
L Kikad_sheet-rescue:APA-106-F5-simons_led D1
U 1 1 618F4614
P 7000 5050
F 0 "D1" H 7000 5417 50  0000 C CNN
F 1 "APA-106-F5" H 7000 5326 50  0000 C CNN
F 2 "LED_SMD:LED_Cree-PLCC4_5x5mm_CW" H 7050 4750 50  0001 L TNN
F 3 "https://cdn.sparkfun.com/datasheets/Components/LED/COM-12877.pdf" H 7100 4675 50  0001 L TNN
F 4 "C114587" H 7000 5050 50  0001 C CNN "LCSC"
	1    7000 5050
	1    0    0    -1  
$EndComp
$Comp
L Kikad_sheet-rescue:APA-106-F5-simons_led D2
U 1 1 618F8CDF
P 8450 5050
F 0 "D2" H 8450 5417 50  0000 C CNN
F 1 "APA-106-F5" H 8450 5326 50  0000 C CNN
F 2 "LED_SMD:LED_Cree-PLCC4_5x5mm_CW" H 8500 4750 50  0001 L TNN
F 3 "https://cdn.sparkfun.com/datasheets/Components/LED/COM-12877.pdf" H 8550 4675 50  0001 L TNN
F 4 "C114587" H 8450 5050 50  0001 C CNN "LCSC"
	1    8450 5050
	1    0    0    -1  
$EndComp
$Comp
L Switch:SW_Push SW1
U 1 1 6189133F
P 3750 4000
F 0 "SW1" H 3750 4285 50  0000 C CNN
F 1 "SW_Push" H 3750 4194 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_Push_1P1T_NO_6x6mm_H9.5mm" H 3750 4200 50  0001 C CNN
F 3 "~" H 3750 4200 50  0001 C CNN
F 4 "C127509" H 3750 4000 50  0001 C CNN "LCSC"
	1    3750 4000
	1    0    0    -1  
$EndComp
$Comp
L Kikad_sheet-rescue:APA-106-F5-simons_led D3
U 1 1 618F99CA
P 9800 5050
F 0 "D3" H 9800 5417 50  0000 C CNN
F 1 "APA-106-F5" H 9800 5326 50  0000 C CNN
F 2 "LED_SMD:LED_Cree-PLCC4_5x5mm_CW" H 9850 4750 50  0001 L TNN
F 3 "https://cdn.sparkfun.com/datasheets/Components/LED/COM-12877.pdf" H 9900 4675 50  0001 L TNN
F 4 "C114587" H 9800 5050 50  0001 C CNN "LCSC"
	1    9800 5050
	1    0    0    -1  
$EndComp
$EndSCHEMATC
