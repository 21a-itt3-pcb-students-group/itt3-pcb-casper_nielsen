#To a small degree inspired Ashish Choudhary from https://circuitdigest.com/microcontroller-projects/diy-raspberry-pi-rgb-lcd-hat
import RPi.GPIO as GPIO
import time
sw1 = 5
sw2 = 6
sw3 = 13
sw4 = 19
sw5 = 26

GPIO.setmode(GPIO.BCM)       # Use BCM GPIO numbers
GPIO.setup(sw1 , GPIO.IN)  
GPIO.setup(sw2 , GPIO.IN) 
GPIO.setup(sw3, GPIO.IN)
GPIO.setup(sw4, GPIO.IN)   
GPIO.setup(sw5, GPIO.IN)  

def button_press():
	 if ( GPIO.input(sw1) == False):
	 	print("button 1 ON")
	 if ( GPIO.input(sw1) == True):
	 	print("button 1 OFF")
	 if ( GPIO.input(sw2) == False):
	 	print("button 2 ON")
	 if ( GPIO.input(sw2) == True):
	 	print("button 2 OFF")
	 if ( GPIO.input(sw3) == False):
	 	print("button 3 ON")
	 if ( GPIO.input(sw3) == True):
	 	print("button 3 OFF")
	 if ( GPIO.input(sw4) == False):
	 	print("button 4 ON")
	 if ( GPIO.input(sw4) == True):
	 	print("button 4 OFF")
	 if ( GPIO.input(sw5) == False):
	 	print("button 5 ON")
	 if ( GPIO.input(sw5) == True):
	 	print("button 5 OFF")

while True:
	button_press()
	time.sleep(0.1)